import java.util.Random;
import java.util.Scanner;

public class Motherboard {
    private GraphicsCard graphicsCard;
    private Processor processor;
    private RAM ram;
    private int motherboardPrice;
    private String motherboardName;

    public Motherboard(GraphicsCard graphicsCard, Processor processor, RAM ram, String motherboardName, int motherboardPrice) {
        this.graphicsCard = graphicsCard;
        this.processor = processor;
        this.ram = ram;
        this.motherboardPrice = motherboardPrice;
        this.motherboardName = motherboardName;
    }

    public Motherboard(String name, int price){
        this.motherboardName = name;
        this.motherboardPrice = price;
    }

    public Motherboard(GraphicsCard graphicsCard, Processor processor, RAM defaultRAM, Source defaultSource, String asus_strix, int i){}

    public GraphicsCard getGraphicsCard(){
        return this.graphicsCard;
    }

    public void setGraphicsCard(GraphicsCard graphicsCard){
        this.graphicsCard = graphicsCard;
    }

    public Processor getProcessor(){
        return this.processor;
    }

    public void setProcessor(Processor processor){
        this.processor = processor;
    }

    public RAM getRam(){
        return this.ram;
    }

    public void setRam(RAM ram){
        this.ram = ram;
    }

    public int getMotherboardPrice(){
        return this.motherboardPrice;
    }

    public void setMotherboardPrice(int motherboardPrice){
        this.motherboardPrice = motherboardPrice;
    }

    public String getMotherboardName(){
        return this.motherboardName;
    }

    public void setMotherboardName(String motherboardName){
        this.motherboardName = motherboardName;
    }

    @Override
    public String toString() {
        return "Motherboard " +
                " motherboardName='" + motherboardName +
                ", motherboardPrice=" + motherboardPrice ;
    }

    public static Motherboard createMotherboard(){
        int totalPrice = 0;
        GraphicsCard graphicsCard = GraphicsCard.chooseGraphicsCard();
        totalPrice = graphicsCard.getPrice();
        System.out.println("Total price so far: " + totalPrice);
        System.out.println("=============================");

        Processor processor = Processor.chooseProcessor();
        totalPrice = totalPrice  + processor.getPrice();
        System.out.println("Total price so far: " + totalPrice);
        System.out.println("=============================");

        RAM ram = RAM.chooseRAM();
        totalPrice = totalPrice + ram.getRamPrice();
        System.out.println("Total price so far: " + totalPrice);
        System.out.println("=============================");

        Motherboard defaultMotherboard = new Motherboard(graphicsCard, processor, ram,"Default", 0);
        Motherboard asusStrix = new Motherboard(graphicsCard, processor, ram,"ASUS Strix", 600);
        Motherboard asusPrime = new Motherboard(graphicsCard, processor, ram,"ASUS Prime", 380);
        Motherboard gigabyteMotherboard = new Motherboard(graphicsCard, processor, ram,"Gygabyte B450M", 445);

        System.out.println("Now choose a motherboard to keep the processor, the Graphics Card and the RAM");
        System.out.println("1. " + asusStrix);
        System.out.println("2. " + asusPrime);
        System.out.println("3. " + gigabyteMotherboard);

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        while (option > 0) {
            if (option == 1) {
                defaultMotherboard = asusStrix;
                defaultMotherboard.motherboardPrice = defaultMotherboard.motherboardPrice + totalPrice;
                System.out.println("You chose " + defaultMotherboard.motherboardName);
                return defaultMotherboard;
            } else if (option == 2) {
                defaultMotherboard = asusPrime;
                defaultMotherboard.motherboardPrice = defaultMotherboard.motherboardPrice + totalPrice;
                System.out.println("You chose " + defaultMotherboard.motherboardName);
                return defaultMotherboard;
            } else if (option == 3) {
                defaultMotherboard = gigabyteMotherboard;
                defaultMotherboard.motherboardPrice = defaultMotherboard.motherboardPrice + totalPrice;
                System.out.println("You chose " + defaultMotherboard.motherboardName);
                return defaultMotherboard;
            } else {
                System.out.println("Invalid option. Try again. ");
                option = scanner.nextInt();
            }
        }
//
//        defaultMotherboard.setGraphicsCard(graphicsCard);
//        defaultMotherboard.setProcessor(processor);
//        defaultMotherboard.setRam(ram);

      return defaultMotherboard;
    }
}
