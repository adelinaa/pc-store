import java.util.Scanner;

public class Source {
    private String sourceName;
    private int sourcePrice;
    private int sourcePower;

    public Source(String sourceName, int sourcePrice, int sourcePower) {
        this.sourceName = sourceName;
        this.sourcePrice = sourcePrice;
        this.sourcePower = sourcePower;
    }

    public Source() {
    }

    public String getSourceName() {
        return this.sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public int getSourcePrice() {
        return this.sourcePrice;
    }

    public void setSourcePrice(int sourcePrice) {
        this.sourcePrice = sourcePrice;
    }

    public int getSourcePower() {
        return this.sourcePower;
    }

    public void setSourcePower(int sourcePower) {
        this.sourcePower = sourcePower;
    }

    @Override
    public String toString() {
        return "Source " +
                "sourceName='" + sourceName + '\'' +
                ", sourcePrice=" + sourcePrice +
                ", sourcePower=" + sourcePower ;
    }

    public static Source chooseSource() {
        Source defaultSource = new Source("Default", 0, 0);
        Source sagetop = new Source("Sagetop 500", 119, 500);
        Source nJoyAyrus = new Source("nJoy Ayrus 500", 154, 500);
        Source nJoyFreya = new Source("nJoy Freya 700", 350, 700);
        System.out.println("==============================");
        System.out.println("Now that the motherboard is ready, let's get you a source.");
        System.out.println("You have the following options. ");
        System.out.println("1. " + sagetop);
        System.out.println("2. " + nJoyAyrus);
        System.out.println("3. " + nJoyFreya);
        System.out.println("Choose your option: ");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        while (option > 0) {
            if (option == 1) {
                defaultSource = sagetop;
                System.out.println("You chose " + defaultSource.sourceName);
                return defaultSource;
            } else if (option == 2) {
                defaultSource = nJoyAyrus;
                System.out.println("You chose " + defaultSource.sourceName);
                return defaultSource;
            } else if (option == 3) {
                defaultSource = nJoyFreya;
                System.out.println("You chose " + defaultSource.sourceName);
                return defaultSource;
            } else {
                System.out.println("Invalid option. Try again. ");
                option = scanner.nextInt();
            }
        }
            return defaultSource;
    }
}


