import java.util.Scanner;

public class Computer {
   private ComputerCase computerCase;
   private Desktop desktop;

    public Computer(ComputerCase computerCase, Desktop desktop) {
        this.computerCase = computerCase;
        this.desktop = desktop;
    }


    public Computer(){}

    public ComputerCase getComputerCase() {
        return computerCase;
    }

    public void setComputerCase(ComputerCase computerCase) {
        this.computerCase = computerCase;
    }

    public Desktop getDesktop() {
        return desktop;
    }

    public void setDesktop(Desktop desktop) {
        this.desktop = desktop;
    }

    public static void createComputer(){
        Customer customer = new Customer();
        double budget = Welcome.welcomeMessage(customer);

        ComputerCase computerCase = ComputerCase.createComputerCase();

        int totalPrice = 0;
        totalPrice = computerCase.getCasePrice();
        System.out.println("Total price so far: " + totalPrice);
        System.out.println("==============================");

        Desktop desktop = Desktop.chooseDesktop();
        totalPrice = totalPrice + desktop.getPrice();


        Computer computer = new Computer();
        computer.setComputerCase(computerCase);
        computer.setDesktop(desktop);

        System.out.println("===================================");
        System.out.println("Your computer has the following components: " + "\n" +
                "Computer Case: " + computerCase.getComputerCaseName() + "\n" +
                "Motherboard: " + computerCase.getMotherboard().getMotherboardName() + "\n" +
                "GraphicsCard: " + computer.getComputerCase().getMotherboard().getGraphicsCard().getGraphicsCardName() + "\n" +
                "Processor: " + computer.getComputerCase().getMotherboard().getProcessor().getProcessorName()  + "\n" +
                "RAM: " + computer.getComputerCase().getMotherboard().getRam().getRamName() + "\n" +
                "Source: " + computer.getComputerCase().getSource().getSourceName() + "\n" +
                "Desktop: " + computer.getDesktop().getName());

        System.out.println("===================================");

        System.out.println("All done. Your total is: " + totalPrice);
        if(budget<totalPrice){
            System.out.println("You are paying an extra of: " + (totalPrice - budget));
        }else if (budget>totalPrice){
            System.out.println("You are saving: " + (budget - totalPrice));
        }else{
            System.out.println("Just the same as your budget. ");
        }

        System.out.println("===================================");

        System.out.println("Pay now? Y/N");
        Scanner scanner = new Scanner(System.in);
        String option = scanner.nextLine().toUpperCase();
        if(option.equals("Y")){
            System.out.println("Your products are on the way. :) ");
        }else{
            System.out.println("Come back whenever you want. ");
        }


    }
}

