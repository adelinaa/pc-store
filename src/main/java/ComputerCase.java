import java.util.Scanner;

public class ComputerCase {
    private Motherboard motherboard;
    private Source source;
    private int casePrice;
    private String computerCaseName;

    public ComputerCase(Motherboard motherboard, Source source, int casePrice, String computerCaseName) {
        this.motherboard = motherboard;
        this.source = source;
        this.casePrice = casePrice;
        this.computerCaseName = computerCaseName;
    }

    public ComputerCase(int casePrice, String computerCaseName){
        this.computerCaseName = computerCaseName;
        this.casePrice = casePrice;
    }

    public Motherboard getMotherboard(){
        return this.motherboard;
    }

    public void setMotherboard(Motherboard motherboard){
        this.motherboard = motherboard;
    }

    public Source getSource(){
        return this.source;
    }

    public void setSource(Source source){
        this.source = source;
    }


    public int getCasePrice(){
        return this.casePrice;
    }

    public void setCasePrice(int casePrice){
        this.casePrice = casePrice;
    }

    public String getComputerCaseName(){
        return this.computerCaseName;
    }

    public void setComputerCaseName(String computerCaseName){
        this.computerCaseName = computerCaseName;
    }

    @Override
    public String toString() {
        return "ComputerCase" +
                ", computerCaseName= " + computerCaseName +
                ", casePrice=" + casePrice;
    }


    public static ComputerCase createComputerCase(){
        int totalPrice = 0;
        Motherboard motherboard = Motherboard.createMotherboard();
        totalPrice =  motherboard.getMotherboardPrice();
        System.out.println("Total price so far: " + totalPrice);

        Source source = Source.chooseSource();
        totalPrice = totalPrice + source.getSourcePrice();

        ComputerCase defaultComputerCase = new ComputerCase(motherboard, source, 0, "Default");
        ComputerCase nJoy = new ComputerCase( motherboard, source, 250, "nJoy Oryn");
        ComputerCase sagetoplux = new ComputerCase( motherboard, source, 240, "Sagetop Lux");
        ComputerCase thermalTake = new ComputerCase(  motherboard, source,350, "Thermaltake H100");

        System.out.println("===============================");
        System.out.println("Almost done. Choose a computer case: ");
        System.out.println("1. " + nJoy);
        System.out.println("2. " + sagetoplux);
        System.out.println("3. " + thermalTake);

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        while (option > 0) {
            if (option == 1) {
                defaultComputerCase = nJoy;
                defaultComputerCase.casePrice = defaultComputerCase.casePrice + totalPrice;
                System.out.println("You chose " + defaultComputerCase.computerCaseName);
                return defaultComputerCase;
            } else if (option == 2) {
                defaultComputerCase = sagetoplux;
                defaultComputerCase.casePrice = defaultComputerCase.casePrice + totalPrice;
                System.out.println("You chose " + defaultComputerCase.computerCaseName);
                return defaultComputerCase;
            } else if (option == 3) {
                defaultComputerCase = thermalTake;
                defaultComputerCase.casePrice = defaultComputerCase.casePrice + totalPrice;
                System.out.println("You chose " + defaultComputerCase.computerCaseName);
                return defaultComputerCase;
            } else {
                System.out.println("Invalid option. Try again. ");
                option = scanner.nextInt();
            }
        }

        return  defaultComputerCase;

    }
}
