import java.util.Scanner;

public class GraphicsCard {
    private String graphicsCardName;
    private int memory;
    private String resolution;
    private String videoProcessor;
    private int price;

    public GraphicsCard(String graphicsCardName, int memory, String resolution, String videoProcessor, int price) {
        this.graphicsCardName = graphicsCardName;
        this.memory = memory;
        this.resolution = resolution;
        this.videoProcessor = videoProcessor;
        this.price = price;
    }

    public GraphicsCard() {
    }

    public String getGraphicsCardName() {
        return graphicsCardName;
    }

    public void setGraphicsCardName(String graphicsCardName) {
        this.graphicsCardName = graphicsCardName;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getVideoProcessor() {
        return videoProcessor;
    }

    public void setVideoProcessor(String videoProcessor) {
        this.videoProcessor = videoProcessor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "GraphicsCard " +
                "graphicsCardName='" + graphicsCardName + '\'' +
                ", memory=" + memory +
                ", resolution='" + resolution + '\'' +
                ", videoProcessor='" + videoProcessor + '\'' +
                ", price=" + price ;
    }

    public static GraphicsCard chooseGraphicsCard(){
        GraphicsCard defaultGraphicsCard = new GraphicsCard("Default", 0, "0", "0", 0);
        GraphicsCard asus = new GraphicsCard("ASUS Radeon", 8, "7680 x 4320", "Radeon RX 570", 829);
        GraphicsCard gigaByte = new GraphicsCard("GYGABYTE AORUS", 8, "7680 x 4320", "GeForce RTX 2080 SUPER", 5000);
        GraphicsCard asusRog = new GraphicsCard("ASUS ROG", 4, "7680 x 4320", "nVidia Geforce GTX 1650", 845);

        System.out.println("Let's begin by choosing a Graphics Card! ");
        System.out.println("Choose from the following: ");
        System.out.println("1. " + asus);
        System.out.println("2. " + gigaByte);
        System.out.println("3. " + asusRog);

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        while (option > 0) {
            if (option == 1) {
                defaultGraphicsCard = asus;
                System.out.println("You chose " + defaultGraphicsCard.graphicsCardName);
                return defaultGraphicsCard;
            } else if (option == 2) {
                defaultGraphicsCard = gigaByte;
                System.out.println("You chose " + defaultGraphicsCard.graphicsCardName);
                return defaultGraphicsCard;
            } else if (option == 3) {
                defaultGraphicsCard = asusRog;
                System.out.println("You chose " + defaultGraphicsCard.graphicsCardName);
                return defaultGraphicsCard;
            } else {
                System.out.println("Invalid option. Try again. ");
                option = scanner.nextInt();
            }
        }
        return defaultGraphicsCard;

    }
}
