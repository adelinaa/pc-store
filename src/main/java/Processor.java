import java.util.Scanner;

public class Processor {
   private String processorName;
   private int cacheMemory;
   private int frequency;
   private int cores;
   private int price;

    public Processor(String processorName, int cacheMemory, int frequency, int cores, int price) {
        this.processorName = processorName;
        this.cacheMemory = cacheMemory;
        this.frequency = frequency;
        this.cores = cores;
        this.price = price;
    }

    public Processor(){}

    public String getProcessorName() {
        return processorName;
    }

    public void setProcessorName(String processorName) {
        this.processorName = processorName;
    }

    public int getCacheMemory() {
        return cacheMemory;
    }

    public void setCacheMemory(int cacheMemory) {
        this.cacheMemory = cacheMemory;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getCores() {
        return cores;
    }

    public void setCores(int cores) {
        this.cores = cores;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Processor " +
                "processorName='" + processorName + '\'' +
                ", cacheMemory=" + cacheMemory +
                ", frequency=" + frequency +
                ", cores=" + cores +
                ", price=" + price ;
    }

    public static Processor chooseProcessor(){

        Processor defaultProcessor = new Processor("Default", 0, 0, 0, 0);
        Processor intelI3 = new Processor("Intel I3", 6, 3600, 4, 369);
        Processor intelI5 = new Processor("Intel I5", 9, 2900, 6, 799);
        Processor intelI7 = new Processor("Intel I7", 12, 3000, 8, 2000);

        System.out.println("Now let's choose a processor.");
        System.out.println("You have the following options: ");
        System.out.println("1. " + intelI3);
        System.out.println("2. " + intelI5);
        System.out.println("3. " + intelI7);

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        while (option > 0) {
            if (option == 1) {
                defaultProcessor = intelI3;
                System.out.println("You chose " + defaultProcessor.processorName);
                return defaultProcessor;
            } else if (option == 2) {
                defaultProcessor = intelI5;
                System.out.println("You chose " + defaultProcessor.processorName);
                return defaultProcessor;
            } else if (option == 3) {
                defaultProcessor = intelI7;
                System.out.println("You chose " + defaultProcessor.processorName);
                return defaultProcessor;
            } else {
                System.out.println("Invalid option. Try again. ");
                option = scanner.nextInt();
            }
        }
        return defaultProcessor;
    }
}
