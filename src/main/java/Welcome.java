import java.util.Scanner;

public class Welcome {
    public static double welcomeMessage(Customer customer) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to our PCs store. ");
        System.out.println("What is your name? ");
        customer.setName(scanner.nextLine());
        System.out.println("What is your budget?");
        customer.setBudget(scanner.nextDouble());
        return customer.getBudget();
    }

}



