import java.util.Scanner;

public class Desktop {
    private String name;
    private double resolution;
    private int price;

    public Desktop(String name, double resolution, int price) {
        this.name = name;
        this.resolution = resolution;
        this.price = price;
    }

    public Desktop(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getResolution() {
        return resolution;
    }

    public void setResolution(double resolution) {
        this.resolution = resolution;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Desktop" +
                "name='" + name + '\'' +
                ", resolution=" + resolution +
                ", price=" + price ;
    }

    public static Desktop chooseDesktop(){
        Desktop defaultDesktop = new Desktop("Default", 0, 0);
        Desktop samsung = new Desktop("Samsung LED", 1920.1080, 700);
        Desktop asusDesktop = new Desktop("Asus LED", 1920.1080, 300);
        Desktop lgDesktop = new Desktop("LG LED", 1920.1080, 200);

        System.out.println("Last but not least, let's get you a desktop: ");
        System.out.println("1. " + samsung);
        System.out.println("2. " + asusDesktop);
        System.out.println("3. " + lgDesktop);

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        while (option > 0) {
            if (option == 1) {
                defaultDesktop = samsung;
                System.out.println("You chose " + defaultDesktop.name);
                return defaultDesktop;
            } else if (option == 2) {
                defaultDesktop = asusDesktop;
                System.out.println("You chose " + defaultDesktop.name);
                return defaultDesktop;
            } else if (option == 3) {
                defaultDesktop = lgDesktop;
                System.out.println("You chose " + defaultDesktop.name);
                return defaultDesktop;
            } else {
                System.out.println("Invalid option. Try again. ");
                option = scanner.nextInt();
            }
        }
        return defaultDesktop;
    }
}
