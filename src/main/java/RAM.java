import java.util.Scanner;

public class RAM {
    private String ramName;
    private String capacity;
    private int ramPrice;

    public RAM(String ramName, String capacity, int ramPrice) {
        this.ramName = ramName;
        this.capacity = capacity;
        this.ramPrice = ramPrice;
    }

    public RAM() {
    }

    public String getRamName() {
        return ramName;
    }

    public void setRamName(String ramName) {
        this.ramName = ramName;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public int getRamPrice() {
        return this.ramPrice;
    }

    public void setRamPrice(int ramPrice) {
        this.ramPrice = ramPrice;
    }

    @Override
    public String toString() {
        return "RAM " +
                "ramName='" + ramName + '\'' +
                ", capacity='" + capacity + '\'' +
                ", ramPrice=" + ramPrice ;
    }

    public static RAM chooseRAM() {

        RAM defaultRAM = new RAM("Default", "0", 0);
        RAM hyperX = new RAM("HyperX Fury Black", "4 GB", 130);
        RAM corsair = new RAM("Corsair Vengeance", "16 GB", 500);
        RAM adata = new RAM("Adata SPECTRIX", "8 GB", 270);

        System.out.println("Now let's choose a RAM memory.");
        System.out.println("You have the following options. ");
        System.out.println("1. " + hyperX);
        System.out.println("2. " + corsair);
        System.out.println("3. " + adata);

        System.out.println("Choose your option: ");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        while (option > 0) {
            if (option == 1) {
                defaultRAM = hyperX;
                System.out.println("You chose " + defaultRAM.ramName);
                return defaultRAM;
            } else if (option == 2) {
                defaultRAM = corsair;
                System.out.println("You chose " + defaultRAM.ramName);
                return defaultRAM;
            } else if (option == 3) {
                defaultRAM = adata;
                System.out.println("You chose " + defaultRAM.ramName);
                return defaultRAM;
            } else {
                System.out.println("Invalid option. Try again. ");
                option = scanner.nextInt();
            }
        }
        return defaultRAM;
    }
}
